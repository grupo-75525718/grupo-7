## Introducción

Hoy en día, existe un creciente interés en el mundo de la tecnología y la computación. Sin embargo, hay quienes, a pesar de su afirmado amor por la tecnología, desconocen aspectos fundamentales como el funcionamiento de la máquina de Turing y su impacto histórico. Es crucial entender que esta máquina, desarrollada hace décadas, sentó las bases de lo que hoy conocemos como computación moderna y resolvió una multitud de problemas complejos en su época.

Conscientes de la importancia de comprender estos conceptos, nos hemos propuesto desarrollar un compilador de lenguaje Turing. Nuestro objetivo es brindar a las personas la oportunidad de explorar y comprender cómo operaban y siguen operando estos principios fundamentales.

## Problema

La problemática a desarrollar es una aplicación que permita compilar y ejecutar máquinas de Turing. Durante el desarrollo, crearemos nuestro propio método para definir una máquina de Turing como código fuente, el cual será el enfoque principal del proyecto.

## Motivación

La motivación detrás de este proyecto radica en la necesidad de crear una herramienta accesible y eficiente que facilite la experimentación con máquinas de Turing. Al proporcionar una manera intuitiva y práctica de definir y ejecutar estas máquinas, se puede fomentar un entendimiento más profundo y aplicado de los principios de la computación.

## Solución

Crear un ejecutable que permita utilizar la máquina de Turing mediante nuestro lenguaje de programación.

## Objetivos

- Definir un lenguaje de representación de la máquina de Turing.
- Generar un ejecutable de la máquina de Turing.
- Generar un grafo mediante la máquina de Turing en el lenguaje DOT.

## Marco Teórico

La evolución de la informática ha sido marcada por avances que han permitido abordar problemas matemáticos y lógicos de manera sistemática. Esta capacidad para resolver problemas complejos mediante algoritmos ejecutados por sistemas computacionales ha sido fundamental en disciplinas como la inteligencia artificial y la ingeniería de software. En este contexto, la conceptualización de sistemas que pueden ejecutar instrucciones precisas para resolver problemas ha sentado las bases para la creación de tecnologías que transforman nuestra interacción con el mundo digital, siendo la máquina de Turing una propuesta que impactó en el área de la computación.

Según Khan y Khayal (2006), la máquina de Turing es un modelo idealizado de computación capaz de almacenar y procesar información virtualmente infinita. Alan M. Turing, su creador, es considerado por muchos como el padre de las ciencias de la computación. Mediante el desarrollo de esta máquina, Turing conceptualizó un modelo teórico con el que buscaba automatizar la resolución de problemas de una manera accesible y sistemática. Se podría decir que la máquina de Turing es un precursor de los computadores de programa almacenado.

## Metodología

### Análisis Sintáctico y Léxico

Para empezar a desarrollar este proyecto, primero debemos escoger el lenguaje de programación que se utilizará. En este caso, haremos uso de Python.

**Instalación de ANTLR:**
```bash
!pip install antlr4-python3-runtime

Luego ejecutamos el siguiente comando para generar el analizador y el lexer en Python a partir de `TuringMachine.g4`:

```bash
antlr4 -Dlanguage=Python3 -visitor -no-listener TuringMachine.g4 -o <ruta/carpeta/output>

Este analizador interpretará el código de la siguiente manera:
```bash
M(Q={A,B} δ={(A,0) -> (A,1, R), (A,1) -> (A,0, R), (A,_) -> (B,_, R)}, A,_, B) input: 1010010_

Donde se declara la lista de estados, las transiciones, el estado inicial, la línea en blanco(que en este caso es un _) , el estado final y la entrada del texto.
## Visitors:
Declaramos las librerías a usar  como antlr4 Y paser y lexer del antlr e implementamos las funciones a usar.

## Generación IR
Desafortunadamente, hubo complicaciones para crear los archivos donde se realiza el CodeGen y no se pudo implementar del todo. Aún así se intentó implementar.
El pseudocódigo que debía de seguir el codigo de llvm era el siguiente:

```bash
Función simulateTuringMachine(transitions, initialState, acceptState, input):
    currentEstado = initialState
    ptr = dirección de input[0]

    Mientras currentEstado no sea igual a acceptState:
        transitionFound := falso

        Para cada transición en transitions:
            Si currentEstado es igual a transition.state y *ptr es igual a transition.symbol:
                currentEstado := transition.willState
                *ptr := transition.willSymbol  // Asignar el primer carácter de willSymbol a ptr

                Si transition.move es "R":
                    ptr := ptr + 1  // Mover el puntero a la derecha
                Sino Si transition.move es "L":
                    ptr := ptr - 1  // Mover el puntero a la izquierda

                transitionFound := verdadero
                Salir del bucle // Salir del bucle for después de encontrar una transición válida

        Si no se encontró ninguna transición válida:
            Imprimir en stderr "No valid transition found for state 'currentEstado' and symbol '*ptr'. Halting."
            Romper // Salir del bucle while

    Devolver input

## Conclusiones:

Si pudimos crear la definición del lenguaje  en máquina de Turing. Todo esto gracias a la ayuda de Antlr. Es una herramienta muy útil para definir correctamente como se declarará la función.
No se pudo llegar al objetivo del ejecutable por las complicaciones de implementación con el LLVM, aun así si implemento en código python el cómo se ejecutaría el código a partir del pseudocódigo planteado. Sin embargo, este proyecto se puede mejorar para futuras investigaciones.
Gracias a la ayuda de Antlr, sí logramos crear el documento DOT para mostrar el gráfico obtenido. Esto por el State_list y Transiciones que actuarán como el objeto y su respectivo label.


