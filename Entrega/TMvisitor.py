# HAsta abajo esta la parte comentada donde se intento implementar el llvm. TAmbien esta lo del optimizador y el JIT
from antlr4 import *
from TuringMachineLexer import TuringMachineLexer
from TuringMachineParser import TuringMachineParser
from TuringMachineVisitor import TuringMachineVisitor

# Clase de tu visitor que hereda de la clase generada por ANTLR
class MyVisitor(TuringMachineVisitor):
    def __init__(self):
        self.start_state = None
        self.blank_symbol = None
        self.accept_state = None
        self.input = None
        self.transitions = []
        self.states = []

    # Método para visitar la descripción de la máquina de Turing
    def visitMachine_description(self, ctx: TuringMachineParser.Machine_descriptionContext):
        self.start_state = ctx.start_state().getText()
        self.blank_symbol = ctx.blank_symbol().getText()
        self.accept_state = ctx.accept_state().getText()
        self.input = ctx.salida().getText() if ctx.salida() else ""
        return self.visitChildren(ctx)

    # Método para visitar las reglas de transición
    def visitTransition(self, ctx: TuringMachineParser.TransitionContext):
        state_from = ctx.estado(0).getText()
        symbol_read = ctx.symbol(0).getText()[0]
        state_to = ctx.estado(1).getText()
        symbol_write = ctx.symbol(1).getText()[0]
        move = ctx.move().getText()

        transition_info = {
            'state_from': state_from,
            'symbol_read': symbol_read,
            'state_to': state_to,
            'symbol_write': symbol_write,
            'move': move
        }

        self.transitions.append(transition_info)
        return self.visitChildren(ctx)

    # Método para visitar la lista de estados
    def visitState_list(self, ctx: TuringMachineParser.State_listContext):
        # Visit each estado in state_list and collect state names
        self.states = [estado.getText() for estado in ctx.estado()]
        return self.states

    # Función para mostrar la información recogida
    def show_machine_info(self):
        print("Start state:", self.start_state)
        print("Blank symbol:", self.blank_symbol)
        print("Accept state:", self.accept_state)
        print("Input:", self.input)
        print("Transitions:")
        for transition in self.transitions:
            print(f"  {transition['state_from']} --({transition['symbol_read']})--> {transition['state_to']} "
                  f"({transition['symbol_write']}, {transition['move']})")

    # Función para simular la máquina de Turing
    def simulate_turing_machine(self):
        current_state = self.start_state
        input_tape = list(self.input)
        ptr = 0

        while current_state != self.accept_state:
            transition_found = False

            for transition in self.transitions:
                if current_state == transition['state_from'] and input_tape[ptr] == transition['symbol_read']:
                    current_state = transition['state_to']
                    input_tape[ptr] = transition['symbol_write']

                    if transition['move'] == 'R':
                        ptr += 1
                    elif transition['move'] == 'L':
                        ptr -= 1

                    transition_found = True
                    break

            if not transition_found:
                print(f"No valid transition found for state '{current_state}' and symbol '{input_tape[ptr]}'. Halting.")
                break

        return ''.join(input_tape)

    # Función para generar el archivo .dot
    def generate_dot_file(self, file_name):
        with open(file_name, 'w') as f:
            f.write("digraph TuringMachine {\n")
            # Write states
            for state in self.states:
                f.write(f'    {state} [shape=circle];\n')

            # Write transitions
            for transition in self.transitions:
                state_from = transition['state_from']
                state_to = transition['state_to']
                symbol_write = transition['symbol_write']
                move = transition['move']
                f.write(f'    {state_from} -> {state_to} [label="{state_to},{symbol_write},{move}"];\n')

            f.write("}\n")

# Función principal para ejecutar el programa
def main():
    # Crear un archivo de entrada (sustituir con tu lógica)
    input_file = FileStream("eample.tm", encoding="utf-8")

    # Inicializar el lexer y parser
    lexer = TuringMachineLexer(input_file)
    stream = CommonTokenStream(lexer)
    parser = TuringMachineParser(stream)
    tree = parser.program()
    
    # Instanciar el visitor
    visitor = MyVisitor()
    
    # Visitar el árbol sintáctico para recoger información
    visitor.visit(tree)

    # Obtener y mostrar la lista de estados
    states = visitor.visitState_list(tree.machine_description().estados().state_list())
    print("States:", states)

    # Mostrar información de la máquina de Turing
    visitor.show_machine_info()

    # Simular la máquina de Turing
    result = visitor.simulate_turing_machine()
    print("Result after simulation:", result)

    # Generar el archivo .dot
    visitor.generate_dot_file("turing_machine.dot")
    print("Dot file generated successfully.")

if __name__ == "__main__":
    main()

# from antlr4 import *
# from llvmlite import ir, binding
# from TuringMachineLexer import TuringMachineLexer
# from TuringMachineParser import TuringMachineParser
# from TuringMachineVisitor import TuringMachineVisitor
# class MyTuringMachineVisitor(TuringMachineVisitor):
  
#     def __init__(self):
#         self.transitions = []
#         self.start_state = None
#         self.blank_symbol = None
#         self.accept_state = None
#         self.input = None
#         self.module = ir.Module(name="TuringMachine")
#         self.builder = None
#         self.func = None
#         self.trans_func = None
#         self.input_ptr = None
  
#     def visitMachine_description(self, ctx: TuringMachineParser.Machine_descriptionContext):
#         self.start_state = ctx.start_state().getText()
#         self.blank_symbol = ctx.blank_symbol().getText()
#         self.accept_state = ctx.accept_state().getText()
#         self.input = ctx.salida().getText() if ctx.salida() else ""

#         #Intento de build
#         ## Initialize LLVM
#         #binding.initialize()
#         #binding.initialize_native_target()
#         #binding.initialize_native_asmprinter()
#       #
#         ## Create main function if it doesn't exist
#         #if not self.builder:
#         #    func_type = ir.FunctionType(ir.VoidType(), [])
#         #    self.func = ir.Function(self.module, func_type, name="main")
#         #    entry_block = self.func.append_basic_block(name="entry")
#         #    self.builder = ir.IRBuilder(entry_block)
#       #
#         ## Create variables
#         #self.input_ptr = self.builder.alloca(ir.IntType(8), name="input_ptr")
#         #self.builder.store(ir.Constant(ir.IntType(8), ord(self.input[0])), self.input_ptr)
#       #
#         ## Visit all transitions
#         #for transition_ctx in ctx.transitions().transition_list().transition():
#         #    self.transitions.append(self.visitTransition(transition_ctx))
#       #
#         ## Generate LLVM code for transitions
#         #self.generateLLVMTransitions()
      
#         return self.transitions, self.start_state, self.blank_symbol, self.accept_state, self.input
  
#     def visitTransition(self, ctx: TuringMachineParser.TransitionContext):
#         state_from = ctx.estado(0).getText()
#         symbol_read = ctx.symbol(0).getText()[0]  
#         state_to = ctx.estado(1).getText()
#         symbol_write = ctx.symbol(1).getText()[0]  
#         move = ctx.move().getText()
      
#         transition_info = {
#             'state_from': state_from,
#             'symbol_read': symbol_read,
#             'state_to': state_to,
#             'symbol_write': symbol_write,
#             'move': move
#         }
      
#         return transition_info
  
#     # def generateLLVMTransitions(self):
#     #     # Inicialización de la variable de estado actual
#     #     currentEstado = self.builder.alloca(ir.IntType(8), name="currentEstado")
#     #     self.builder.store(ir.Constant(ir.IntType(8), ord(self.start_state)), currentEstado)
#     #     # Bucle principal para simular la máquina de Turing
#     #     while_block = self.builder.append_basic_block(name="while_loop")
#     #     end_block = self.builder.append_basic_block(name="end")
#     #     self.builder.branch(while_block)
#     #     self.builder.position_at_end(while_block)
#     #     current_char = self.builder.load(self.input_ptr)
#     #     ptr_value = self.builder.ptrtoint(self.input_ptr, ir.IntType(64))
#     #     for transition in self.transitions:
#     #         state_from = self.builder.alloca(ir.IntType(8), name="state_from")
#     #         self.builder.store(ir.Constant(ir.IntType(8), ord(transition["state_from"])), state_from)
#     #         symbol_read = self.builder.alloca(ir.IntType(8), name="symbol_read")
#     #         self.builder.store(ir.Constant(ir.IntType(8), ord(transition["symbol_read"])), symbol_read)
#     #         with self.builder.if_then(
#     #             self.builder.and_(
#     #                 self.builder.icmp_signed("==", self.builder.load(currentEstado), self.builder.load(state_from)),
#     #                 self.builder.icmp_signed("==", self.builder.load(current_char), self.builder.load(symbol_read))
#     #             )
#     #         ):
#     #             state_to = self.builder.alloca(ir.IntType(8), name="state_to")
#     #             self.builder.store(ir.Constant(ir.IntType(8), ord(transition["state_to"])), state_to)
#     #             symbol_write = self.builder.alloca(ir.IntType(8), name="symbol_write")
#     #             self.builder.store(ir.Constant(ir.IntType(8), ord(transition["symbol_write"])), symbol_write)
#     #             self.builder.store(self.builder.load(state_to), currentEstado)
#     #             self.builder.store(self.builder.load(symbol_write), current_char)
#     #             if transition["move"] == "R":
#     #                 self.builder.store(self.builder.add(ptr_value, ir.Constant(ir.IntType(64), 1)), self.input_ptr)
#     #             elif transition["move"] == "L":
#     #                 self.builder.store(self.builder.sub(ptr_value, ir.Constant(ir.IntType(64), 1)), self.input_ptr)
#     #             self.builder.branch(end_block)
#     #     self.builder.position_at_end(end_block)
#     #     self.builder.store(self.builder.load(self.input_ptr), current_char)
#     #     # Imprimir el estado final del input después de la ejecución
#     #     print_final = self.builder.call(
#     #         self.builder.module.declare_intrinsic('llvm.printf', [ir.IntType(8).as_pointer()], ir.IntType(32)),
#     #         [ir.Constant(ir.IntType(8).as_pointer(), b"Input final: %s\n\00"), self.builder.load(self.input_ptr)]
#     #     )
#     #     self.builder.ret(ir.Constant(ir.IntType(32), 0))
    #   def optimize(self):
    #     llvm_ir = str(self.module)
    #     llvm_module = binding.parse_assembly(llvm_ir)
    #     llvm_module.verify()

    #     # Create a target machine representing the host
    #     target = binding.Target.from_default_triple()
    #     target_machine = target.create_target_machine()

    #     # Create a pass manager and add some optimizations
    #     pmb = binding.PassManagerBuilder()
    #     pmb.opt_level = 3

    #     pm = binding.ModulePassManager()
    #     pm.add_function_inlining_pass(225)
    #     pmb.populate(pm)

    #     pm.run(llvm_module)

    #     optimized_ir = str(llvm_module)
    #     return optimized_ir
    # def jit_compile_and_execute(self):
    #    # Optimizar y obtener el IR optimizado
    #    optimized_ir = self.optimize()

        # Crear un motor de compilador MCJIT
    #    binding.initialize_all_targets()
    #    binding.initialize_all_asmprinters()
    #    target = Target.from_default_triple()
    #    target_machine = target.create_target_machine()
    #    with Context() as context:
    #        with Module.from_ir_string(optimized_ir) as llvm_module:
                # Crear un motor de ejecución MCJIT
    #            with create_mcjit_compiler(llvm_module, target_machine) as ee:
                    # Obtener la función principal y ejecutarla
    #                func_ptr = ee.get_function_address("main")
    #               cfunc = CFUNCTYPE(None)(func_ptr)
    #                cfunc()

# if __name__ == '__main__':
#     file_name = "eample.txt"
  
#     input_stream = FileStream(file_name, encoding="utf-8")
#     lexer = TuringMachineLexer(input_stream)
#     token_stream = CommonTokenStream(lexer)
#     parser = TuringMachineParser(token_stream)
#     tree = parser.program()  
  
#     visitor = MyTuringMachineVisitor()
#     llvm_ir = visitor.visit(tree)

#     with open("ejecutable.ll", "w") as f:
#        f.write(str(llvm_ir))
    

