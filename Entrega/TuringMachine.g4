grammar TuringMachine;

program: machine_description ;

machine_description: 'M(' estados ',' transitions ',' start_state ',' blank_symbol ',' accept_state ') input:' salida;

estados: 'Q=' '{' state_list '}';
transitions: 'δ={' transition_list '}';
start_state: ID;
blank_symbol: SYMBOL;
accept_state: ID;

transition_list: transition (',' transition)*;
state_list: estado (',' estado)*;
symbol_list: symbol (',' symbol)*;

transition: '(' estado ',' symbol ')' '->' '(' estado ',' symbol ',' move ')';
estado: ID;
salida: SYMBOL;
symbol: SYMBOL;
move: 'R' | 'L';

ID: [A-Z][a-zA-Z0-9]*;
SYMBOL: [a-z0-9_]+;
WS: [ \t\r\n]+ -> skip;